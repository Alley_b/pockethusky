
/*
Using google maps' Javascript API and phonegap's geolocation plugin display a
map of your current area with a marker that follows you position
*/

//global variables for latitude and longitude and an object for the marker
//they need to be accessed by many times by onMapWatchSuccess()
var Latitude = undefined;
var Longitude = undefined;
var marker = undefined;
var userLatLng = undefined;
var infoWindowContent = undefined;
var checkpoints = undefined;
//function that draws the map initially in onSuccess, sets initial map center
//and marker position
function getMap(latitude, longitude) {

   var mapOptions = {
       center: new google.maps.LatLng(0, 0),
       zoom: 1,
       mapTypeId: google.maps.MapTypeId.ROADMAP
   };

   map = new google.maps.Map
   (document.getElementById("map"), mapOptions);

   //array of checkpoint objects, contains position LatLng, location name,
   //check-in range, and info window description
   checkpoints = [
     {
       position: new google.maps.LatLng(47.76148281, -122.18973993),
       location: "Veterans Archway",
       range: 10,
       infoWindowContent: '<div class="info_content">' +
        "<h3>Veteran's Archway</h3>" +
        "<p>Veteran's Archway is a monument celebrating veterans' transition from active duty to student and civilian life</p>" +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76316961, -122.19186526),
       location: 'Sarah Simonds Green Conservatory',
       range: 20,
       infoWindowContent: '<div class="info_content">' +
        '<h3>SSG Conservatory</h3>' +
        '<p>The Sarah Simonds Green Consevatory is a sustainable and energy-efficent green house and classroom surrounded by raised garden beds</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76000393, -122.19070683),
       location: 'Campus Security',
       range: 10,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Camupus Security</h3>' +
        '<p>The office for campus saftey and security</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76082134, -122.18779471),
       location: 'Wetlands Boardwalk',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Wetlands Boardwalk</h3>' +
        '<p>Boardwalk into the Wetlands</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76027054, -122.19033706),
       location: 'North Creek Events Center',
       range: 30,
       infoWindowContent: '<div class="info_content">' +
        '<h3>North Creek Events Center</h3>' +
        '<p>The North Creek Events Center is a large room for holding conferences and other large events</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76017232, -122.19238299),
       location: 'Ancestors',
       range: 30,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Wooden Carvings</h3>' +
        '<p>Campus art piece carved from tree trunks</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76424452, -122.19373648),
       location: 'Beardslee Building',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Beardslee Building</h3>' +
        '<p>Furthest building on campus, contains most of the School of Business classrooms and offices</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76011693, -122.19119984),
       location: 'Food for Thought/UW Bookstore',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Food for Thought/UW Bookstore</h3>' +
        "<p>Food for Thought Cafe along with UW Bothell's own bookstore, come here for all your class textbooks, class supplies, and UW merch</p>" +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76041406, -122.18967322),
       location: 'Sports Field',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Sports Field</h3>' +
        '<p>all the UW Bothell Sports fields, inculdes, Baseball, Basketball, tennis, beach volleyball and others</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75966619, -122.19299587),
       location: 'Truly House',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Truly House</h3>' +
        '<p>House containing several faculty offices</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7604814, -122.19170583),
       location: 'Public Garden',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Public Garden</h3>' +
        '<p>Public Garden open to all students, take what you want</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76164006, -122.19491653),
       location: 'Husky Hall',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Husky Village</h3>' +
        '<p>Husky Hall contains various faculty offices and sudent services</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7590949, -122.1908041),
       location: 'Student Success Center',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Student Success Center</h3>' +
        '<p>The home of student advising and other services such as disablity resources, merit scholarships, and more</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75900092, -122.19112757),
       location: 'IDEA Project',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>IDEA Project</h3>' +
        '<p>An open space for students to meet, colaborate and feel welcome</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7589567, -122.1906292),
       location: 'Pool Table',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Pool Table</h3>' +
        '<p>Pool table open to students to play</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7591282, -122.1906978),
       location: 'Counseling Center',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Counseling Center</h3>' +
        '<p>The Counseling Center provides Students with numerous services including confidential short-term counseling, same-day crisis appointments, individual couseling and more</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75900079, -122.19111943),
       location: 'Ping Pong Table',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Ping Pong Table</h3>' +
        '<p>Ping pong table open to the students to play</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75865217, -122.19072998),
       location: 'Business and IAS Offices',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Buisness and IAS Offices</h3>' +
        '<p>Offices foe The Schools of Business and IAS</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7602342, -122.1912062),
       location: 'IT Center',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>IT Center</h3>' +
        '<p>The IT center provides many services to students and faculty all of which are available from the helpdesk</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.759638, -122.190953),
       location: 'IT Offices',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>IT Offices</h3>' +
        '<p>The Offices for the IT Department</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7596895, -122.1913706),
       location: 'Library Offices',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Library Offices</h3>' +
        '<p>Offices for Library faculty</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75968692, -122.19108172),
       location: 'Info Commons',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Info Commons</h3>' +
        '<p>Get help for anything library related here</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75982179, -122.19138048),
       location: 'Reading Room',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Reading Room</h3>' +
        '<p>A room where students can read in guarenteed silence</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7602189, -122.1911867),
       location: 'Study Areas 1',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Study Areas</h3>' +
        '<p>A place for students to sit and study along with a collection of rooms that can be reserved by students for sudying or group meetings</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7599781, -122.191476),
       location: 'Study Areas 2',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Study Areas</h3>' +
        '<p>A place for students to sit and study along with a collection of rooms that can be reserved by students for sudying or group meetings</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75946509, -122.19113448),
       location: 'Subway',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Subway</h3>' +
        "<p>Our Campus's very own Subway</p>" +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.76023338, -122.19042896),
       location: 'Gym',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Gym</h3>' +
        '<p>The ARC gym where students and staff can get active and exercise</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7599844, -122.1904634),
       location: 'ARC 1st Floor',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Video Game Alcove</h3>' +
        '<p>Floor with video game alcove, foosball tables, Student Involvement & Leadership Offices and Community Resource Center</p>' +
        '</div>'
     },

     {
       position: new google.maps.LatLng(47.76018145, -122.19050305),
       location: 'Multipurpose Event and Gathering Space (ARC 2nd Floor)',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Multipurpose Event and Gathering Space (ARC 2nd Floor)</h3>' +
        '<p>Place foe students to gather in the open desk space, private meeting rooms, or hold larger events</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7590118, -122.1917195),
       location: 'Open Learning Lab',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Open Learning Lab</h3>' +
        '<p>Cmoputer lab with PC and mac computers with digital media software</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7590153, -122.1917425),
       location: 'Makerspace/Digital Media Lab',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Digital Media Lab</h3>' +
        '<p>Lab with 24 mac computers with professional digital media software, for classroom teaching and open student use, also home of the Digital Future Lab, Loacted next to the Makerspace, a creative space for students to study and explore with special equipment including 3D printers and workshops to learn how to use them</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7587837, -122.1915237),
       location: 'Writing & Communication Center',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Writing & Communication Center</h3>' +
        '<p>A resource for students to get peer consultation and help with reading, writing, and presenting</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.758814, -122.1915625),
       location: 'Faculty Offices',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Faculty Offices</h3>' +
        '<p>Area with many faculty offices</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7584976, -122.1912731),
       location: 'Common Grounds',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Common Grounds</h3>' +
        '<p>A place for students to meet, eat, and grab a coffee</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7588547, -122.1915022),
       location: 'Quantitative Skills Center',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Quantitative Skills Center</h3>' +
        '<p>A resource for students to get help and peer tutoring for any quantitative skills, such as math and programming</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75920378, -122.1914213),
       location: 'Lecture Hall',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Lecture Hall</h3>' +
        '<p>Largest classroom/lecture hall on campus</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7590259, -122.1918115),
       location: 'School of STEM Student Services',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>School of STEM Student Services</h3>' +
        '<p>Office for students who need any services regarding the School of STEM</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75899572, -122.19192323),
       location: 'School of STEM Faculty Offices',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>School of STEM Faculty Offices</h3>' +
        '<p>Offices for STEM faculty</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.7589737, -122.1918754),
       location: 'Labs Floor 2',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Labs floor 2</h3>' +
        '<p>Labs floor 2</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75936379, -122.19148039),
       location: 'Labs Floor 3',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Labs Floor 3</h3>' +
        '<p>Labs Floor 3</p>' +
        '</div>'
     },
     {
       position: new google.maps.LatLng(47.75899572, -122.19192323),
       location: 'Labs Floor 4',
       range: 15,
       infoWindowContent: '<div class="info_content">' +
        '<h3>Labs Floor 4</h3>' +
        '<p>Labs Floor 4</p>' +
        '</div>'
     },

   ];

  //custom marker images and sizes
  var  image = {
    url: "img/marker2.png",
    scaledSize: new google.maps.Size(50, 75)
   };

   //for each loop that places checkpoint markers, and sets up infowindows
   checkpoints.forEach(function(point){
     var markerPoint = new google.maps.Marker({
       position: point.position,
       map:map,
       icon: image,
      //  shape: shape,
       title: point.location
     });

     var infoWindow = new google.maps.InfoWindow({
       content: point.infoWindowContent
     });

     markerPoint.addListener('click', function(){
       infoWindow.open(map, markerPoint);
       checkDist(point.position, point.range);
     });
   });

  //setting user marker
  var latLng = new google.maps.LatLng(latitude, longitude);
  userLatLng = new google.maps.LatLng(latitude, longitude);
   marker = new google.maps.Marker({
       position: latLng
   });

   marker.setMap(map);
   map.setZoom(18);
   map.setCenter(marker.getPosition());
}

//computes distance between 2 LatLng objects and returns as an int
function computeDist(p1, p2)
{
  return google.maps.geometry.spherical.computeDistanceBetween(p1, p2).toFixed(2);
}

//checks distance between user position and shows an alert if in or out of range
function checkDist(center, range)
{
  if(computeDist(userLatLng, center) <= range)
  {
    alert("in range " + computeDist(userLatLng, center));
  }
  else
  {
    alert("not in range " + computeDist(userLatLng, center));
  }
}

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
       // app.receivedEvent('deviceready');
       navigator.geolocation.getCurrentPosition(app.onSuccess, app.onError, { enableHighAccuracy: true });
       var watchID = navigator.geolocation.watchPosition(app.onMapWatchSuccess, app.onError, { enableHighAccuracy: true });

    },

    //event fired when getCurrentPosition is successful, gets coordinates and plugs
    //them into getMap
    onSuccess: function(position){
        longitude = position.coords.longitude;
        latitude = position.coords.latitude;

        getMap(latitude, longitude);
    },

    //event fired if there is an error with getCurrentPosition or watchPosition
    onError: function(error){
        alert("the code is " + error.code + ". \n" + "message: " + error.message);
    },

    //event fired when watchPosition is successful, checks if latitude and longitude
    //have changed from their previous values and if they are different it changes their
    //values to the new location and moves the marker to the new position
    onMapWatchSuccess: function(position) {

      var updatedLatitude = position.coords.latitude;
      var updatedLongitude = position.coords.longitude;

      //check if lattitude and longitude has changed and update if changed
      if (updatedLatitude != Latitude && updatedLongitude != Longitude) {

          Latitude = updatedLatitude;
          Longitude = updatedLongitude;

          var newLatLng = new google.maps.LatLng(updatedLatitude, updatedLongitude);
          userLatLng = new google.maps.LatLng(updatedLatitude, updatedLongitude);
          marker.setPosition(newLatLng);
      }
    },
};

app.initialize();
